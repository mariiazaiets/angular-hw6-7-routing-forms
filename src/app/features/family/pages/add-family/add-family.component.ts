import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { FamilyService } from '../../services/family.service';

@Component({
  selector: 'lab-js-add-family',
  templateUrl: './add-family.component.html',
  styleUrls: ['./add-family.component.scss']
})
export class AddFamilyComponent implements OnInit {
  public familyForm: FormGroup;

  public get children() {
    return this.familyForm.get('children') as FormArray;
  }

  public constructor(
    private readonly familyService: FamilyService
  ) {}

  public ngOnInit(): void {
    this.initForm()
  }

  public addChild(): void {
    const child = this.generateChild()
    this.children.push(child)
  }

  public removeChild(index: number): void {
    this.children.removeAt(index)
  }

  public generateChild(): FormGroup {
    return new FormGroup({
      name: new FormControl(null, [Validators.required]),
      age: new FormControl(null, [Validators.required]),
    })
  }

  public submit() {
    this.familyService.addFamily$(this.familyForm.value)
    console.log('Family added successfully!')
    this.familyForm.reset();
  }

  private initForm(): void {
    this.familyForm = new FormGroup({
      id: new FormControl(null),
      name: new FormControl(null, [Validators.required]),
      father: new FormGroup({
        name: new FormControl(null, [Validators.required]),
        age: new FormControl(null, [Validators.required]),
      }),
      mother: new FormGroup({
        name: new FormControl(null, [Validators.required]),
        age: new FormControl(null, [Validators.required]),
      }),
      children: new FormArray([
        this.generateChild()
      ])
    })
  }
}
