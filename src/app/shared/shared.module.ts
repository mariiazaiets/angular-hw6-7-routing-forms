import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const PUBLIC_COMPONENTS: never[] = [];
const PUBLIC_DIRECTIVES: never[] = [];
const PUBLIC_PIPES: never[] = [];

@NgModule({
  declarations: [
    ...PUBLIC_COMPONENTS,
    ...PUBLIC_DIRECTIVES,
    ...PUBLIC_PIPES,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CommonModule,
    ...PUBLIC_COMPONENTS,
    ...PUBLIC_DIRECTIVES,
    ...PUBLIC_PIPES,
  ],
})
export class SharedModule { }
